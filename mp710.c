// ---------------------------------------------------------------------------
// php module for mp710 device interoperations
// (device homepage: http://www.masterkit.ru/main/set.php?code_id=573112)
// (c) Anton Shmigirilov <shmigirilov@gmail.com>
// ---------------------------------------------------------------------------
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif
// ---------------------------------------------------------------------------
#include "php.h"
#include "php_mp710.h"
#include "hidapi.h"
// ---------------------------------------------------------------------------
static zend_class_entry *php_mp710_sc_entry;
zend_object_handlers mp710_object_handlers;
ZEND_DECLARE_MODULE_GLOBALS(mp710)
static PHP_GINIT_FUNCTION(mp710);

#define MP710_VID (0x16c0)
#define MP710_PID (0x05df)

#define FEATURE_BUF_SIZE (9)
#define EEPROM_WAIT (5)

#define CODE_CTL_WRITE      (0x63)
#define CODE_CTL_READ       (0x36)
#define CODE_INFO_READ      (0x1D)
#define CODE_EEPROM_WRITE   (0x0E)
#define CODE_EEPROM_READ    (0xE0)

#define EEPROM_ADDR_CNT     (0x0A)
#define EEPROM_ADDR_START   (0x30)
#define EEPROM_ADDR_END     (0xFF)

typedef struct _php_mp710_object
{
    zend_object zo;
    hid_device *handle;
}
php_mp710_object;
// ---------------------------------------------------------------------------
// PHP class constructor
// prototype: mp710::_construct()
PHP_METHOD(mp710, __construct)
{
    php_mp710_object *mp710_obj;

    // try to find our device
    struct hid_device_info *dev = hid_enumerate(MP710_VID, MP710_PID);
    if (dev == NULL)
    {
        zend_throw_exception(zend_exception_get_default(TSRMLS_C), "MP710: device not found", 0 TSRMLS_CC);
        RETURN_FALSE;
    }

    hid_free_enumeration(dev);

    // store device handler into internal object storage
    mp710_obj = (php_mp710_object *)zend_object_store_get_object(getThis() TSRMLS_CC);

    mp710_obj->handle = hid_open(MP710_VID, MP710_PID, NULL);
    if (mp710_obj->handle == NULL)
    {
        zend_throw_exception(zend_exception_get_default(TSRMLS_C), "MP710: failed to open device", 0 TSRMLS_CC);
        RETURN_FALSE;
    }
}
// ---------------------------------------------------------------------------
static int mp_set(hid_device *handle, int num, int reg, int com, int cmd, int prg)
{
    int ret;
    unsigned char buf[FEATURE_BUF_SIZE];

    memset(buf, 0, sizeof(buf));

    buf[1] = CODE_CTL_WRITE;
    buf[2] = num;
    buf[3] = reg;
    buf[4] = com;
    buf[5] = cmd >> 8;
    buf[6] = cmd & 0xFF;
    buf[7] = prg >> 8;
    buf[8] = prg & 0xFF;

    ret = hid_send_feature_report(handle, buf, FEATURE_BUF_SIZE);
    if (ret != FEATURE_BUF_SIZE)
        return -1;

    ret = hid_get_feature_report(handle, buf, FEATURE_BUF_SIZE);
    if (ret < 0)
        return -1;

    if ( (buf[1] != CODE_CTL_WRITE) ||
        (buf[2] != num) ||
        (buf[3] != reg) ||
        (buf[4] != com) ||
        (buf[5] != cmd >> 8) ||
        (buf[6] != cmd & 0xFF) ||
        (buf[7] != prg >> 8) ||
        (buf[8] != prg & 0xFF)
    )
    {
        return -1;
    }
    
    return 0;
}
// ---------------------------------------------------------------------------
static int mp_get(hid_device *handle, int num, int *reg, int *com, int *cmd, int *prg)
{
    int ret;
    unsigned char buf[FEATURE_BUF_SIZE];

    memset(buf, 0, sizeof(buf));

    buf[1] = CODE_CTL_READ;
    buf[2] = num;

    ret = hid_send_feature_report(handle, buf, FEATURE_BUF_SIZE);
    if (ret != FEATURE_BUF_SIZE)
        return -1;

    ret = hid_get_feature_report(handle, buf, FEATURE_BUF_SIZE);
    if (ret < 0)
        return -1;

    if ( (buf[1] != CODE_CTL_READ) ||
        (buf[2] != num)
    )
    {
        return -1;
    }

    *reg = buf[3];
    *com = buf[4];
    *cmd = (buf[5] << 8) | buf[6];
    *prg = (buf[7] << 8) | buf[8];
    
    return 0;
}
// ---------------------------------------------------------------------------
// PHP class method returns unique device id
// prototype: int mp710::dev_id()
PHP_METHOD(mp710, dev_id)
{
    int ret;
    unsigned char buf[FEATURE_BUF_SIZE];

    php_mp710_object *mp710_obj;
    zval *object = getThis();
    mp710_obj = (php_mp710_object *)zend_object_store_get_object(object TSRMLS_CC);

    if (mp710_obj->handle == NULL)
    {
        zend_throw_exception(zend_exception_get_default(TSRMLS_C), "MP710: device is not opened", 0 TSRMLS_CC);
        RETURN_FALSE;
    }

    if (zend_parse_parameters_none() == FAILURE)
        RETURN_FALSE;

    memset(buf, 0, sizeof(buf));

    buf[1] = CODE_INFO_READ;
    ret = hid_send_feature_report(mp710_obj->handle, buf, FEATURE_BUF_SIZE);
    if (ret != FEATURE_BUF_SIZE)
    {
        zend_throw_exception(zend_exception_get_default(TSRMLS_C), "MP710: I/O error (1)", 0 TSRMLS_CC);
        RETURN_FALSE;
    }

    ret = hid_get_feature_report(mp710_obj->handle, buf, FEATURE_BUF_SIZE);
    if (ret < 0)
    {
        zend_throw_exception(zend_exception_get_default(TSRMLS_C), "MP710: I/O error (2)", 0 TSRMLS_CC);
        RETURN_FALSE;
    }

    if (buf[1] != CODE_INFO_READ)
    {
        zend_throw_exception(zend_exception_get_default(TSRMLS_C), "MP710: I/O error (3)", 0 TSRMLS_CC);
        RETURN_FALSE;
    }
        
    RETURN_LONG( (buf[5] << 24) | (buf[6] << 16) | (buf[7] << 8) | buf[8] );
}
// ---------------------------------------------------------------------------
// PHP class method returns device software version
// prototype: int mp710::softv()
PHP_METHOD(mp710, softv)
{
    int ret;
    unsigned char buf[FEATURE_BUF_SIZE];

    php_mp710_object *mp710_obj;
    zval *object = getThis();
    mp710_obj = (php_mp710_object *)zend_object_store_get_object(object TSRMLS_CC);

    if (mp710_obj->handle == NULL)
    {
        zend_throw_exception(zend_exception_get_default(TSRMLS_C), "MP710: device is not opened", 0 TSRMLS_CC);
        RETURN_FALSE;
    }

    if (zend_parse_parameters_none() == FAILURE)
        RETURN_FALSE;

    memset(buf, 0, sizeof(buf));

    buf[1] = CODE_INFO_READ;
    ret = hid_send_feature_report(mp710_obj->handle, buf, FEATURE_BUF_SIZE);
    if (ret != FEATURE_BUF_SIZE)
    {
        zend_throw_exception(zend_exception_get_default(TSRMLS_C), "MP710: I/O error (1)", 0 TSRMLS_CC);
        RETURN_FALSE;
    }

    ret = hid_get_feature_report(mp710_obj->handle, buf, FEATURE_BUF_SIZE);
    if (ret < 0)
    {
        zend_throw_exception(zend_exception_get_default(TSRMLS_C), "MP710: I/O error (2)", 0 TSRMLS_CC);
        RETURN_FALSE;
    }

    if (buf[1] != CODE_INFO_READ)
    {
        zend_throw_exception(zend_exception_get_default(TSRMLS_C), "MP710: I/O error (3)", 0 TSRMLS_CC);
        RETURN_FALSE;
    }

    RETURN_LONG( buf[3] | (buf[4] << 8) );
}
// ---------------------------------------------------------------------------
// PHP class method returns device family
// prototype: int mp710::family()
PHP_METHOD(mp710, family)
{
    int ret;
    unsigned char buf[FEATURE_BUF_SIZE];

    php_mp710_object *mp710_obj;
    zval *object = getThis();
    mp710_obj = (php_mp710_object *)zend_object_store_get_object(object TSRMLS_CC);

    if (mp710_obj->handle == NULL)
    {
        zend_throw_exception(zend_exception_get_default(TSRMLS_C), "MP710: device is not opened", 0 TSRMLS_CC);
        RETURN_FALSE;
    }

    if (zend_parse_parameters_none() == FAILURE)
        RETURN_FALSE;

    memset(buf, 0, sizeof(buf));

    buf[1] = CODE_INFO_READ;
    ret = hid_send_feature_report(mp710_obj->handle, buf, FEATURE_BUF_SIZE);
    if (ret != FEATURE_BUF_SIZE)
    {
        zend_throw_exception(zend_exception_get_default(TSRMLS_C), "MP710: I/O error (1)", 0 TSRMLS_CC);
        RETURN_FALSE;
    }

    ret = hid_get_feature_report(mp710_obj->handle, buf, FEATURE_BUF_SIZE);
    if (ret < 0)
    {
        zend_throw_exception(zend_exception_get_default(TSRMLS_C), "MP710: I/O error (2)", 0 TSRMLS_CC);
        RETURN_FALSE;
    }

    if (buf[1] != CODE_INFO_READ)
    {
        zend_throw_exception(zend_exception_get_default(TSRMLS_C), "MP710: I/O error (3)", 0 TSRMLS_CC);
        RETURN_FALSE;
    }

    RETURN_LONG( buf[2] );
}
// ---------------------------------------------------------------------------
// PHP class method returns device manufacturer
// prototype: int mp710::manufacturer()
PHP_METHOD(mp710, manufacturer)
{
    #define MAX_STR 255
    wchar_t wstr[MAX_STR];
    unsigned char mbstr[MAX_STR * 2];
    int ret;

    php_mp710_object *mp710_obj;
    zval *object = getThis();
    mp710_obj = (php_mp710_object *)zend_object_store_get_object(object TSRMLS_CC);

    if (mp710_obj->handle == NULL)
    {
        zend_throw_exception(zend_exception_get_default(TSRMLS_C), "MP710: device is not opened", 0 TSRMLS_CC);
        RETURN_FALSE;
    }

    if (zend_parse_parameters_none() == FAILURE)
        RETURN_FALSE;

    wstr[0] = 0x0000;
    ret = hid_get_manufacturer_string(mp710_obj->handle, wstr, MAX_STR);
    if (ret < 0)
    {
        zend_throw_exception(zend_exception_get_default(TSRMLS_C), "MP710: I/O error", 0 TSRMLS_CC);
        RETURN_FALSE;
    }

    // convert wide USB string to multibyte representation
    ret = wcstombs(mbstr, wstr, MAX_STR*2);
    if (ret < 0)
    {
        zend_throw_exception(zend_exception_get_default(TSRMLS_C), "MP710: multibyte conversion error", 0 TSRMLS_CC);
        RETURN_FALSE;
    }

    RETURN_STRING(mbstr, 1);
}
// ---------------------------------------------------------------------------
// PHP class method returns device product string
// prototype: int mp710::product()
PHP_METHOD(mp710, product)
{
    #define MAX_STR 255
    wchar_t wstr[MAX_STR];
    unsigned char mbstr[MAX_STR * 2];
    int ret;

    php_mp710_object *mp710_obj;
    zval *object = getThis();
    mp710_obj = (php_mp710_object *)zend_object_store_get_object(object TSRMLS_CC);

    if (mp710_obj->handle == NULL)
    {
        zend_throw_exception(zend_exception_get_default(TSRMLS_C), "MP710: device is not opened", 0 TSRMLS_CC);
        RETURN_FALSE;
    }

    if (zend_parse_parameters_none() == FAILURE)
        RETURN_FALSE;

    wstr[0] = 0x0000;
    ret = hid_get_product_string(mp710_obj->handle, wstr, MAX_STR);
    if (ret < 0)
    {
        zend_throw_exception(zend_exception_get_default(TSRMLS_C), "MP710: I/O error", 0 TSRMLS_CC);
        RETURN_FALSE;
    }

    // convert wide USB string to multibyte representation
    ret = wcstombs(mbstr, wstr, MAX_STR*2);
    if (ret < 0)
    {
        zend_throw_exception(zend_exception_get_default(TSRMLS_C), "MP710: multibyte conversion error", 0 TSRMLS_CC);
        RETURN_FALSE;
    }

    RETURN_STRING(mbstr, 1);
}
// ---------------------------------------------------------------------------
static int ee_write(hid_device *handle, unsigned char addr, unsigned char data)
{
    int ret;
    unsigned char buf[FEATURE_BUF_SIZE];

    memset(buf, 0, sizeof(buf));

    buf[1] = CODE_EEPROM_WRITE;
    buf[2] = addr;
    buf[3] = data;

    ret = hid_send_feature_report(handle, buf, FEATURE_BUF_SIZE);
    if (ret != FEATURE_BUF_SIZE)
        return -1;

    // we need to wait some time to eeprom processing
    Sleep(EEPROM_WAIT);

    ret = hid_get_feature_report(handle, buf, FEATURE_BUF_SIZE);
    if (ret < 0)
        return -1;

    if ( (buf[1] != CODE_EEPROM_WRITE) ||
        (buf[2] != addr) ||
        (buf[3] != data)
    )
    {
        return -1;
    }

    return 0;
}
// ---------------------------------------------------------------------------
// PHP class method writes byte to device's EEPROM
// prototype: bool mp710::eeprom_write(addr, data)
PHP_METHOD(mp710, eeprom_write)
{
    int addr, data;
    int ret;

    php_mp710_object *mp710_obj;
    zval *object = getThis();
    mp710_obj = (php_mp710_object *)zend_object_store_get_object(object TSRMLS_CC);

    if (mp710_obj->handle == NULL)
    {
        zend_throw_exception(zend_exception_get_default(TSRMLS_C), "MP710: device is not opened", 0 TSRMLS_CC);
        RETURN_FALSE;
    }

    if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "ll", &addr, &data) == FAILURE)
        return;

    ret = ee_write(mp710_obj->handle, addr, data);
    if (ret < 0)
    {
        zend_throw_exception(zend_exception_get_default(TSRMLS_C), "MP710: I/O error", 0 TSRMLS_CC);
        RETURN_FALSE;
    }

    RETURN_TRUE;
}
// ---------------------------------------------------------------------------
// PHP class method read byte from device's EEPROM
// prototype: int mp710::eeprom_read(addr)
PHP_METHOD(mp710, eeprom_read)
{
    int addr;
    int ret;
    unsigned char buf[FEATURE_BUF_SIZE];

    php_mp710_object *mp710_obj;
    zval *object = getThis();
    mp710_obj = (php_mp710_object *)zend_object_store_get_object(object TSRMLS_CC);

    if (mp710_obj->handle == NULL)
    {
        zend_throw_exception(zend_exception_get_default(TSRMLS_C), "MP710: device is not opened", 0 TSRMLS_CC);
        RETURN_FALSE;
    }

    if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "l", &addr) == FAILURE)
        return;

    memset(buf, 0, sizeof(buf));

    buf[1] = CODE_EEPROM_READ;
    buf[2] = addr;

    ret = hid_send_feature_report(mp710_obj->handle, buf, FEATURE_BUF_SIZE);
    if (ret != FEATURE_BUF_SIZE)
    {
        zend_throw_exception(zend_exception_get_default(TSRMLS_C), "MP710: I/O error (1)", 0 TSRMLS_CC);
        RETURN_FALSE;
    }

    ret = hid_get_feature_report(mp710_obj->handle, buf, FEATURE_BUF_SIZE);
    if (ret < 0)
    {
        zend_throw_exception(zend_exception_get_default(TSRMLS_C), "MP710: I/O error (2)", 0 TSRMLS_CC);
        RETURN_FALSE;
    }

    if ( (buf[1] != CODE_EEPROM_READ) ||
        (buf[2] != addr)
    )
    {
        zend_throw_exception(zend_exception_get_default(TSRMLS_C), "MP710: I/O error (3)", 0 TSRMLS_CC);
        RETURN_FALSE;
    }

    RETURN_LONG(buf[3]);
}
// ---------------------------------------------------------------------------
// PHP class method for EEPROM loading
// prototype: bool mp710::eeprom_load(Array bytes)
PHP_METHOD(mp710, eeprom_load)
{
    php_mp710_object *mp710_obj;
    zval *object = getThis();
    zval *arr;
    zval **data;
    HashTable *arr_hash;
    HashPosition pointer;
    int array_count, i, ret;
    mp710_obj = (php_mp710_object *)zend_object_store_get_object(object TSRMLS_CC);

    if (mp710_obj->handle == NULL)
    {
        zend_throw_exception(zend_exception_get_default(TSRMLS_C), "MP710: device is not opened", 0 TSRMLS_CC);
        RETURN_FALSE;
    }

    if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "a", &arr) == FAILURE)
        return;

    arr_hash = Z_ARRVAL_P(arr);
    array_count = zend_hash_num_elements(arr_hash);

    i = 0;
    for(zend_hash_internal_pointer_reset_ex(arr_hash, &pointer);
        zend_hash_get_current_data_ex(arr_hash, (void**) &data, &pointer) == SUCCESS;
        zend_hash_move_forward_ex(arr_hash, &pointer))
    {
        if (Z_TYPE_PP(data) == IS_LONG) 
        {
            ret = ee_write(mp710_obj->handle, EEPROM_ADDR_START + i, Z_LVAL_PP(data));
            if (ret < 0)
            {
                zend_throw_exception(zend_exception_get_default(TSRMLS_C), "MP710: I/O error", 0 TSRMLS_CC);
                RETURN_FALSE;
            }
            
            i++;
        }
    }

    ret = ee_write(mp710_obj->handle, EEPROM_ADDR_CNT, i / 36);
    if (ret < 0)
    {
        zend_throw_exception(zend_exception_get_default(TSRMLS_C), "MP710: I/O error", 0 TSRMLS_CC);
        RETURN_FALSE;
    }

    RETURN_TRUE;
}
// ---------------------------------------------------------------------------
// PHP class method for EEPROM erasing
// prototype: bool mp710::eeprom_erase()
PHP_METHOD(mp710, eeprom_erase)
{
    int i, ret;

    php_mp710_object *mp710_obj;
    zval *object = getThis();
    mp710_obj = (php_mp710_object *)zend_object_store_get_object(object TSRMLS_CC);

    if (mp710_obj->handle == NULL)
    {
        zend_throw_exception(zend_exception_get_default(TSRMLS_C), "MP710: device is not opened", 0 TSRMLS_CC);
        RETURN_FALSE;
    }

    if (zend_parse_parameters_none() == FAILURE)
        RETURN_FALSE;

    ret = ee_write(mp710_obj->handle, EEPROM_ADDR_CNT, 0);
    if (ret < 0)
    {
        zend_throw_exception(zend_exception_get_default(TSRMLS_C), "MP710: I/O error", 0 TSRMLS_CC);
        RETURN_FALSE;
    }

    for (i = EEPROM_ADDR_START; i < EEPROM_ADDR_END; i++)
    {
        ret = ee_write(mp710_obj->handle, i, 0xFF);
        if (ret < 0)
        {
            zend_throw_exception(zend_exception_get_default(TSRMLS_C), "MP710: I/O error", 0 TSRMLS_CC);
            RETURN_FALSE;
        }
    }

    RETURN_TRUE;
}
// ---------------------------------------------------------------------------
// PHP class method for specified port switching to on state
// prototype: bool mp710::port_on(num)
PHP_METHOD(mp710, port_on)
{
    int num, ret;

    php_mp710_object *mp710_obj;
    zval *object = getThis();
    mp710_obj = (php_mp710_object *)zend_object_store_get_object(object TSRMLS_CC);

    if (mp710_obj->handle == NULL)
    {
        zend_throw_exception(zend_exception_get_default(TSRMLS_C), "MP710: device is not opened", 0 TSRMLS_CC);
        RETURN_FALSE;
    }

    if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "l", &num) == FAILURE)
        return;

    ret = mp_set(mp710_obj->handle, num, 128, 0, 15, 15);
    if (ret < 0)
    {
        zend_throw_exception(zend_exception_get_default(TSRMLS_C), "MP710: I/O error", 0 TSRMLS_CC);
        RETURN_FALSE;
    }

    RETURN_TRUE;
}
// ---------------------------------------------------------------------------
// PHP class method for specified port switching to off state
// prototype: bool mp710::port_off(num)
PHP_METHOD(mp710, port_off)
{
    int num, ret;

    php_mp710_object *mp710_obj;
    zval *object = getThis();
    mp710_obj = (php_mp710_object *)zend_object_store_get_object(object TSRMLS_CC);

    if (mp710_obj->handle == NULL)
    {
        zend_throw_exception(zend_exception_get_default(TSRMLS_C), "MP710: device is not opened", 0 TSRMLS_CC);
        RETURN_FALSE;
    }

    if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "l", &num) == FAILURE)
        return;

    ret = mp_set(mp710_obj->handle, num, 0, 0, 15, 15);
    if (ret < 0)
    {
        zend_throw_exception(zend_exception_get_default(TSRMLS_C), "MP710: I/O error", 0 TSRMLS_CC);
        RETURN_FALSE;
    }

    RETURN_TRUE;
}
// ---------------------------------------------------------------------------
PHP_METHOD(mp710, __call)
{
    char *name;
    int name_len, ret, array_count;
    int num, reg, com, cmd, prg;
    zval **data;
    HashTable *arr_hash;
    HashPosition pointer;
    zval *args;
    php_mp710_object *mp710_obj;
    zval *object = getThis();
    mp710_obj = (php_mp710_object *)zend_object_store_get_object(object TSRMLS_CC);

    if (mp710_obj->handle == NULL)
    {
        zend_throw_exception(zend_exception_get_default(TSRMLS_C), "MP710: device is not opened", 0 TSRMLS_CC);
        RETURN_FALSE;
    }

    if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "sa", &name, &name_len, &args) == FAILURE)
        return;

    name[name_len] = 0;

    if (!strcmp(name, "set"))
    {
        int i = 0;
        arr_hash = Z_ARRVAL_P(args);
        array_count = zend_hash_num_elements(arr_hash);

        for(zend_hash_internal_pointer_reset_ex(arr_hash, &pointer);
            zend_hash_get_current_data_ex(arr_hash, (void**) &data, &pointer) == SUCCESS;
            zend_hash_move_forward_ex(arr_hash, &pointer))
        {
            if (Z_TYPE_PP(data) == IS_LONG) 
            {
                switch (i)
                {
                    case 0:
                        num = Z_LVAL_PP(data);
                        break;
                    case 1:
                        reg = Z_LVAL_PP(data);
                        break;
                    case 2:
                        com = Z_LVAL_PP(data);
                        break;
                    case 3:
                        cmd = Z_LVAL_PP(data);
                        break;
                    case 4:
                        prg = Z_LVAL_PP(data);
                        break;
                }
            }

            i++;
        }

        ret = mp_set(mp710_obj->handle, num, reg, com, cmd, prg);
        if (ret < 0)
        {
            zend_throw_exception(zend_exception_get_default(TSRMLS_C), "MP710: I/O error", 0 TSRMLS_CC);
            RETURN_FALSE;
        }

        RETURN_TRUE;
    } // if name=="set"
    else if (!strcmp(name, "get"))
    {
        arr_hash = Z_ARRVAL_P(args);
        array_count = zend_hash_num_elements(arr_hash);

        zend_hash_internal_pointer_reset_ex(arr_hash, &pointer);
        zend_hash_get_current_data_ex(arr_hash, (void**) &data, &pointer);

        num = Z_LVAL_PP(data);

        ret = mp_get(mp710_obj->handle, num, &reg, &com, &cmd, &prg);
        if (ret < 0)
        {
            zend_throw_exception(zend_exception_get_default(TSRMLS_C), "MP710: I/O error", 0 TSRMLS_CC);
            RETURN_FALSE;
        }

        array_init(return_value);

        add_assoc_long(return_value, "reg", reg);
        add_assoc_long(return_value, "com", com);
        add_assoc_long(return_value, "cmd", cmd);
        add_assoc_long(return_value, "prg", prg);
        
        return;
    }

    RETURN_FALSE;
}
// ---------------------------------------------------------------------------
PHP_METHOD(mp710, __callstatic)
{
    char *name;
    int name_len, ret, array_count;
    int num, reg, com, cmd, prg;
    hid_device *handle;
    zval **data;
    HashTable *arr_hash;
    HashPosition pointer;
    zval *args;
    
    // try to find our device
    struct hid_device_info *dev = hid_enumerate(MP710_VID, MP710_PID);
    if (dev == NULL)
    {
        zend_throw_exception(zend_exception_get_default(TSRMLS_C), "MP710: device not found", 0 TSRMLS_CC);
        RETURN_FALSE;
    }

    hid_free_enumeration(dev);

    handle = hid_open(MP710_VID, MP710_PID, NULL);
    if (handle == NULL)
    {
        zend_throw_exception(zend_exception_get_default(TSRMLS_C), "MP710: failed to open device", 0 TSRMLS_CC);
        RETURN_FALSE;
    }

    if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "sa", &name, &name_len, &args) == FAILURE)
    {
        hid_close(handle);
        return;
    }

    name[name_len] = 0;

    if (!strcmp(name, "set"))
    {
        int i = 0;
        
        arr_hash = Z_ARRVAL_P(args);
        array_count = zend_hash_num_elements(arr_hash);

        for(zend_hash_internal_pointer_reset_ex(arr_hash, &pointer);
            zend_hash_get_current_data_ex(arr_hash, (void**) &data, &pointer) == SUCCESS;
            zend_hash_move_forward_ex(arr_hash, &pointer))
        {
            if (Z_TYPE_PP(data) == IS_LONG) 
            {
                switch (i)
                {
                    case 0:
                        num = Z_LVAL_PP(data);
                        break;
                    case 1:
                        reg = Z_LVAL_PP(data);
                        break;
                    case 2:
                        com = Z_LVAL_PP(data);
                        break;
                    case 3:
                        cmd = Z_LVAL_PP(data);
                        break;
                    case 4:
                        prg = Z_LVAL_PP(data);
                        break;
                }
            }

            i++;
        }
        
        ret = mp_set(handle, num, reg, com, cmd, prg);
        if (ret < 0)
        {
            zend_throw_exception(zend_exception_get_default(TSRMLS_C), "MP710: I/O error", 0 TSRMLS_CC);
            hid_close(handle);
            RETURN_FALSE;
        }

        hid_close(handle);

        RETURN_TRUE;
    } // if name=="set"
    else if (!strcmp(name, "get"))
    {
        arr_hash = Z_ARRVAL_P(args);
        array_count = zend_hash_num_elements(arr_hash);

        zend_hash_internal_pointer_reset_ex(arr_hash, &pointer);
        zend_hash_get_current_data_ex(arr_hash, (void**) &data, &pointer);

        num = Z_LVAL_PP(data);

        ret = mp_get(handle, num, &reg, &com, &cmd, &prg);
        if (ret < 0)
        {
            zend_throw_exception(zend_exception_get_default(TSRMLS_C), "MP710: I/O error", 0 TSRMLS_CC);
            hid_close(handle);
            RETURN_FALSE;
        }

        array_init(return_value);

        add_assoc_long(return_value, "reg", reg);
        add_assoc_long(return_value, "com", com);
        add_assoc_long(return_value, "cmd", cmd);
        add_assoc_long(return_value, "prg", prg);

        hid_close(handle);
        return;        
    }

    hid_close(handle);
    RETURN_FALSE;
}
// ---------------------------------------------------------------------------
ZEND_BEGIN_ARG_INFO(arginfo_mp710_void, 0)
ZEND_END_ARG_INFO()
// ---------------------------------------------------------------------------
ZEND_BEGIN_ARG_INFO(arginfo_mp710_eeprom_write, 2)
    ZEND_ARG_INFO(0, addr)
    ZEND_ARG_INFO(0, data)
ZEND_END_ARG_INFO()
// ---------------------------------------------------------------------------
ZEND_BEGIN_ARG_INFO(arginfo_mp710_eeprom_read, 1)
    ZEND_ARG_INFO(0, addr)
ZEND_END_ARG_INFO()
// ---------------------------------------------------------------------------
ZEND_BEGIN_ARG_INFO(arginfo_mp710_eeprom_load, 1)
    ZEND_ARG_INFO(0, bytes)
ZEND_END_ARG_INFO()
// ---------------------------------------------------------------------------
ZEND_BEGIN_ARG_INFO(arginfo_mp710_port_onoff, 1)
    ZEND_ARG_INFO(0, num)
ZEND_END_ARG_INFO()
// ---------------------------------------------------------------------------
ZEND_BEGIN_ARG_INFO(arginfo_mp710_call, 2)
    ZEND_ARG_INFO(0, name)
    ZEND_ARG_INFO(0, args)
ZEND_END_ARG_INFO()
// ---------------------------------------------------------------------------
static zend_function_entry php_mp710_class_methods[] = {
    PHP_ME(mp710,       __construct,        arginfo_mp710_void, ZEND_ACC_PUBLIC|ZEND_ACC_CTOR)
    PHP_ME(mp710,       __call,             arginfo_mp710_call, ZEND_ACC_PUBLIC)
    PHP_ME(mp710,       __callstatic,       arginfo_mp710_call, ZEND_ACC_PUBLIC|ZEND_ACC_STATIC)
    PHP_ME(mp710,       dev_id,             arginfo_mp710_void, ZEND_ACC_PUBLIC)
    PHP_ME(mp710,       softv,              arginfo_mp710_void, ZEND_ACC_PUBLIC)
    PHP_ME(mp710,       family,             arginfo_mp710_void, ZEND_ACC_PUBLIC)
    PHP_ME(mp710,       manufacturer,       arginfo_mp710_void, ZEND_ACC_PUBLIC)
    PHP_ME(mp710,       product,            arginfo_mp710_void, ZEND_ACC_PUBLIC)
    PHP_ME(mp710,       eeprom_write,       arginfo_mp710_eeprom_write, ZEND_ACC_PUBLIC)
    PHP_ME(mp710,       eeprom_read,        arginfo_mp710_eeprom_read, ZEND_ACC_PUBLIC)
    PHP_ME(mp710,       eeprom_load,        arginfo_mp710_eeprom_load, ZEND_ACC_PUBLIC)
    PHP_ME(mp710,       eeprom_erase,       arginfo_mp710_void, ZEND_ACC_PUBLIC)
    PHP_ME(mp710,       port_on,            arginfo_mp710_port_onoff, ZEND_ACC_PUBLIC)
    PHP_ME(mp710,       port_off,           arginfo_mp710_port_onoff, ZEND_ACC_PUBLIC)
    PHP_FE_END
};
// ---------------------------------------------------------------------------
static void php_mp710_object_free_storage(void *object TSRMLS_DC)
{
    php_mp710_object *mp710_obj = (php_mp710_object *)object;

    if (!mp710_obj)
        return;

    // release hidapi device if needed
    if (mp710_obj->handle)
        hid_close(mp710_obj->handle);
    
    zend_object_std_dtor(&mp710_obj->zo TSRMLS_CC);
    efree(mp710_obj);
}
// ---------------------------------------------------------------------------
static zend_object_value php_mp710_object_new(zend_class_entry *class_type TSRMLS_DC)
{
    zend_object_value retval;
    php_mp710_object *mp710_obj;

    mp710_obj = emalloc(sizeof(php_mp710_object));
    memset(mp710_obj, 0, sizeof(php_mp710_object));

    zend_object_std_init(&mp710_obj->zo, class_type TSRMLS_CC);
    object_properties_init(&mp710_obj->zo, class_type);

    retval.handle = zend_objects_store_put(mp710_obj, NULL, (zend_objects_free_object_storage_t) php_mp710_object_free_storage, NULL TSRMLS_CC);
    retval.handlers = (zend_object_handlers *) &mp710_object_handlers;

    return retval;
}
// ---------------------------------------------------------------------------
PHP_MINIT_FUNCTION(mp710)
{
    zend_class_entry ce;

    hid_init();

    memcpy(&mp710_object_handlers, zend_get_std_object_handlers(), sizeof(zend_object_handlers));

    INIT_CLASS_ENTRY(ce, "mp710", php_mp710_class_methods);
    ce.create_object = php_mp710_object_new;
    mp710_object_handlers.clone_obj = NULL;
    php_mp710_sc_entry = zend_register_internal_class(&ce TSRMLS_CC);

    return SUCCESS;
}
// ---------------------------------------------------------------------------
PHP_MSHUTDOWN_FUNCTION(mp710)
{
    hid_exit();
    return SUCCESS;
}
// ---------------------------------------------------------------------------
PHP_MINFO_FUNCTION(mp710)
{
    php_info_print_table_start();
    php_info_print_table_header(2, "MP710 device support", "enabled");
    php_info_print_table_row(2, "MP710 module version", PHP_MP710_VERSION);
    php_info_print_table_end();
}
// ---------------------------------------------------------------------------
static PHP_GINIT_FUNCTION(mp710)
{
    memset(mp710_globals, 0, sizeof(*mp710_globals));
}
// ---------------------------------------------------------------------------
zend_module_entry mp710_module_entry = {
    STANDARD_MODULE_HEADER,
    "mp710",
    NULL,
    PHP_MINIT(mp710),
    PHP_MSHUTDOWN(mp710),
    NULL,
    NULL,
    PHP_MINFO(mp710),
    PHP_MP710_VERSION,
    PHP_MODULE_GLOBALS(mp710),
    PHP_GINIT(mp710),
    NULL,
    NULL,
    STANDARD_MODULE_PROPERTIES_EX
};
// ---------------------------------------------------------------------------
#ifdef COMPILE_DL_MP710
ZEND_GET_MODULE(mp710)
#endif
// ---------------------------------------------------------------------------
// EOF
