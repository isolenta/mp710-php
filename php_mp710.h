// ---------------------------------------------------------------------------
#ifndef PHP_MP710_H
#define PHP_MP710_H
// ---------------------------------------------------------------------------
#define PHP_MP710_VERSION "1.0"
#define PHP_MP710_EXTNAME "mp710"
// ---------------------------------------------------------------------------
extern zend_module_entry mp710_module_entry;
#define phpext_mp710_ptr &mp710_module_entry

ZEND_BEGIN_MODULE_GLOBALS(mp710)
    char *extension_dir;
ZEND_END_MODULE_GLOBALS(mp710)
// ---------------------------------------------------------------------------
#endif // PHP_MP710_H
// ---------------------------------------------------------------------------
// EOF
