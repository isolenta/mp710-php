==============================================================================
This extension implements new PHP class named 'mp710'
==============================================================================
Class mp710 has following set of public methods:

mp710::__construct() (or explicit calling via class instantiation):
    Try to find device connected and open it. Throws exception if failed.

bool set($num, $reg, $com, $cmd, $prg)
    Set device's control registers

Array get($num)
    Get device's control registers values 
    as associative array with keys named 'reg', 'com', 'cmd', 'prg'

int mp710::dev_id()
    Returns device's unique id.

int mp710::softv()
    Returns device's software version

int mp710::family()
    Returns device's family code

string mp710::manufacturer()
    Returns device's manufacturer string

string mp710::product()
    Returns device's product string

bool mp710::port_on($num)
    Switch specified port to on state

bool mp710::port_off($num)
    Switch specified port to off state

bool mp710::eeprom_write($addr, $data)
    Write byte to device's EEPROM to given address

int mp710::eeprom_read($addr)
    Read byte from device's EEPROM by given address

bool mp710::eeprom_load(Array $bytes)
    Load data array into device's EEPROM

bool mp710::eeprom_erase()
    Erase device's EEPROM data

==============================================================================
Class has following static methods similiar to corresponding object's methods:

static bool mp710::set($num, $reg, $com, $cmd, $prg)
static Array mp710::get($num)


==============================================================================
All methods will be throw exception in case of some sorts of I/O errors
==============================================================================

You are able to use only one device instance at time. So, you need to unset
existing instance of class mp710 before creating a new one,
or before using static methods
