PHP_ARG_ENABLE(mp710, whether to enable mp710 support,
[ --enable-mp710 Enable mp710 support])

if test "$PHP_MP710" = "yes"; then
AC_DEFINE(HAVE_MP710, 1, [Whether you have mp710])
PHP_NEW_EXTENSION(mp710, mp710.c, $ext_shared)
fi
